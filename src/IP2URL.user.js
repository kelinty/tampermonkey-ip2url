// ==UserScript==
// @name         IP2URL
// @namespace    https://gitlab.com/kelinty/tampermonkey-ip2url
// @version      1.3.2
// @description  Makes URL from IP
// @author       Pavel Salkov
// @match        *://*.server/*
// @match        *://*/zabbix/*
// @match        *://*.local/*
// @icon         https://gitlab.com/kelinty/tampermonkey-ip2url/-/raw/main/src/icon.ico
// @grant        none
// ==/UserScript==

function addSshLink(){
    [
        'table.wikitable  td', // wiki
        'table.list-table td', // wiki
        'ul.table-forms table td', // Zabbix
        'ul.table-forms div.interface-cell-ip', // Zabbix
        'table#servers_list td>div>a', // Servers List (bitrix)
        'div#workarea-content table td > small[title]', // Special (bitrix)
        'div.content-container span[data-test-id="IP Addresses:"]', // vCenter
        'div.datagrid-table vsc-dg-cell-container span' // vCenter
    ].forEach(function(sel) {
        ReplaceByRegex(sel, /^(?:\s*)(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?:(?::?\d*)|\b)(?!.*"sshproto")/, '$&' + link('$1') ); // 127.0.0.1:12345 or 127.0.0.1
        ReplaceByRegex(sel, /^(?:\s*)<input .[^<>]*value\="(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})".[^<>]*[\/]?>(?:\s*)$/, '$&' + link('$1')); // <input ..... "127.0.0.1"...>
    });
}

function link(v){
    return '<a class="sshproto" title="ssh://' + v + '" href="ssh://' + v + '">&#x1F3F4;</a><a class="sshproto" title="rdp://' + v + '" href="rdp://' + v + '">&#x1F5D4;</a>';
}

function ReplaceByRegex(selector, filter, rep){
    var objects = document.querySelectorAll(selector);
    objects.forEach(function(link) {
        if(filter.test(link.innerHTML)){
            link.innerHTML = link.innerHTML.replace(filter, rep);
        }
    });
}

(function() {
    var style = document.createElement('style');
    style.innerHTML = '.sshproto {}';
    //style.innerHTML = '.sshproto {float:right; position: absolute; z-index:999;}';
    document.head.appendChild(style);

    // On AJAX
    const send = XMLHttpRequest.prototype.send
    XMLHttpRequest.prototype.send = function() {
        this.addEventListener('load', addSshLink());
        return send.apply(this, arguments);
    }

    // On Load
    addSshLink();
})();
