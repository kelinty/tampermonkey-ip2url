# tampermonkey-ip2url
add clickable links to IP-addresses

# Installation
- Install Add-on: https://addons.mozilla.org/ru/firefox/addon/tampermonkey/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search
- Tab "Utilities" in Dashboard of Tampermonkey addon
- Import from URL https://gitlab.com/kelinty/tampermonkey/-/raw/main/src/IP2URL.user.js